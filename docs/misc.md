# Misc.

Editing / Adding Taxonomy Terms
Taxonomy Terms are used to connect, relate, and classify content on the website.
1) From the top Admin menu: Structure > Taxonomy > [category you wish to edit] > Edit.
 A screenshot of a cell phone

Description automatically generated 

2) Click ‘List’ tab.
3) Click ‘Add term’ button to add new taxonomy term.
4) Click arrow to the right of the ‘Edit’ button to open dropdown and select Delete to delete taxonomy term.
5) Click ‘Save’ to save your work.
* The only taxonomy category that is currently using the Edit function is Historical; see Historical Fact > Editing the Timeline Title. 
Editing Custom Blocks
Blocks are boxes of content that are displayed on specified pages. They are currently being used in the Major Transfer Information content type (Transfer Course Criteria pages) and at the bottom of the newsletter. Edits made to the custom blocks will appear on all pages that have that block on it, e.g. edits made to the Major Transfer Top Content block will appear on all ~80 Transfer Course Criteria pages.


Major Transfer Top Content
 A screenshot of a social media post

Description automatically generated 

Newsletter Bottom Block Content
 A person in a suit and tie

Description automatically generated 

1) From the top Admin menu: Structure > Block Layout > Custom Block Library.
2) Click ‘Edit’ button of the block you wish to edit (Major Transfer Top Content or Newsletter Bottom Block Content).
3) Make your edits in the WYSIWYG screen; the Newsletter Bottom Block Content also has an image field.
4) Click ‘Save’ to save your work.

