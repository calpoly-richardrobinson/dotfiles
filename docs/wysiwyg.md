# WYSIWYG Field Styles, Formats and Options

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

Many content types and components in the new website contain WYSIWYG fields, in which text and content is styled in much the same manner as it is in Word. Explained here are only the options which may be a bit different and so require some explanation. 
(A) Horizontal Rule Button

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

To insert a Horizontal Rule, simply click on the button indicated with an (A) in the first screenshot in this section.
(B) Add/Remove Link Button

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

1) Highlight the text you want to create a link with and click the indicated icon. (Note: To remove the link, highlight it then click the grayed-out icon to the right of the one indicated above.)

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

2) If you are linking to a page within the site, begin typing the name of the page and then select it from the dropdown. If you are linking to an external site, enter the URL (including http:// or https://).
Button Links

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

Above are all of the button links styles available, with the name they are listed by in the Styles dropdown.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

1) Highlight the text you want to create a link with.
2) Click ‘Add Link’ button and enter the URL (see above if you need more information).
3) With the link text still selected (and now underlined to show that the link has been added), select the button style you want from the Styles dropdown (full name of the style will appear in a tag upon hover).
(C) List Button
Simple unordered (or bulleted) and ordered lists are created just like they are in Word: create a list, highlight it, and then click the icon for either unordered or unordered. In the Cal Poly site, however, there are two different styles available to you for lists. 
Link List

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

A link list lets the user know that the links will go different file types, and inserts an icon to indicate what the file type is. 

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

1) Enter your list of files and highlight them.
2) Click the ‘Unordered List’ icon.
3) Create your links. (See Add/Remove Link. You will need to have uploaded your files already in order to select them; see Adding Media to the Site > Files/Media.) Then click on the ‘Ordered List’ icon.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

4) Select ‘Link List’ from the Styles dropdown.
Step-by-Step List

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

The Step-by-Step list is created in the same way as the Link List, with the following differences:
1) Instead of highlighting your list of items and then clicking on the ‘Unordered List’ icon, click on the one to the right: ‘Ordered List’

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

2) Select ‘Stepped list’ from the Styles dropdown.
(D) Blockquote Button

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

Blockquotes can be a good way to break up content on a page, especially in news stories. There’s a simple way to style a blockquote (block is slightly indented; the large open quote mark is added – the top example), and another that requires you to switch to source mode in the WYSIWYG editor and add a few HTML tags (block is slightly indented; name is styled, as well as the following line – the second example above).
Simple Blockquote

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

1) Enter your text.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

2) Highlight the text you want to be styled.
3) Click ‘Blockquote’ button. This will add the large open quote (see next screenshot); if you are creating a simple blockquote this is all you need to do.
Styled Blockquote

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

4) Click ‘Source’ button to edit HTML markup / add tags to style the text.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

5) Add <cite> tag before the name of the person quoted and </cite> tag before the </p> tag.
6) Add <em> tag before first word in the second line (this may not always be degree and year) and </em> tag before the </cite> tag.
7) Click ‘Source’ button again to get back into WYSIWYG mode.
(E) Image Button

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

Images added using the WYSIWYG can be styled and sized: the image above was added and then placed on the right side of the page, set to take up 50% of the content area.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

1) Place your cursor where you want your image to be on the page (in this example, at the top of the page*) and click the ‘Image’ button.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

2) Click ‘Choose file’ button, then on your computer find and then select the image you want to upload to the site. (Note: a red asterisk next to a field name indicates it is a required field.)
3) Enter Alternative text.
4) Click ‘Save’.
* If your image is already at the size you want it you can set the alignment here. If you set alignment this way you cannot use the Style function explained in numbers 5 and 6 below.
** Check ‘Caption’ if you want to add a caption below your image (not shown in this example). The caption, which would be added in the next screen, appears below the image in smaller text.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

5) Click on your image to select it.
6) From the Styles dropdown, select the orientation and size for your image. (Note:  when you hover over the selection a tag telling you the size will appear – this is Image Right 50 Widget).

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*
 
The image will look a little different in the WYSIWYG now – not exactly as it will on the published page, but you can see that it is smaller and the position has shifted. When you save the page and view it, it will appear as it does in the first Image Button screenshot.


(F) Embed Media Button
This button is not being used: use the Embed YouTube Video Button instead.
(G) Accordion (Collapsible Item) Button

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

Accordions are most ideal when the audience only requires a few key pieces of content on a single page. By hiding most of the content, users can scan the page and more quickly find the specific content they are looking for.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

1) With your cursor in the WYSIWYG field where you want to place the accordion, click the ‘Accordion’ button.
2) Enter text that is always visible in the top field, typing over ‘Header goes here’.
3) Enter text that will be hidden in the field below, typing over ‘Content goes here’. Note that this field can contain formatted text, images, etc. 
4) To add another accordion or more content below the one you have created, hover over the bottom right corner of the accordion and then click the red ‘return arrow’ button that appears. 
(H) Callout Button

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

Callouts are used to direct your audience’s attention to important information, with different styles for different uses. The process for creating them is very similar to the one to used to create accordions.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

1) With your cursor in the WYSIWYG field where you want to place the, click the ‘Callout’ button.
2) Select the type of Callout you want from the dropdown.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

3) Enter Callout Title, typing over text displayed in the WYSIWYG.
4) Enter Callout Content, typing over text displayed in the WYSIWYG. Note that this field can contain formatted text, images, etc.
 (I) Embed YouTube Video Button
 A group of people posing for a photo

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

Videos hosted on YouTube may be added to pages on the site.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

1) With your cursor in the WYSIWYG field where you want to place the video, click the ‘Embed YouTube Video’ button. This will bring up a modal window in which you will have to enter the URL for the video from YouTube.
2) Before entering anything in the modal window, find the video you want to add to your page on YouTube.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

3) Click on ‘Share’ button below the YouTube Video and to the right (difficult to see in this screenshot!).
4) Copy the video URL.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

5) Paste the URL in the Paste YouTube Video URL field.
6) Enter ‘1600’ into Width field and ‘900’ into Height field. 
7) Check ‘Make Responsive’ box.
8) Click ‘Okay’.
(J) Table Button

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

Above is a very simple table. You can create different styles of tables by clicking on ‘Source’ and adding classes. 

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

1) With your cursor in the WYSIWYG field where you want to place your table, click ‘Table’ button.
2) Enter the number of rows and columns you would like in your table. 
3) If you would like a header, select from the dropdown (options: None, First Row, First Column, Both).
4) Enter (optional) Caption, which will appear above the table.
5) Enter (optional) Summary. This text will not display, but is a place where you can annotate the HTML – this will be visible when you toggle to ‘Source’.
The basic table style is striped: you should in most cases leave the other fields -- Border Size,  Alignment, Width, Height, Cell spacing, Cell padding -- alone.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

6) Click into the cell you want to add text to and enter your copy.
 (K) Styles Dropdown

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

As demonstrated in other area of this document (Step-by-Step List, Button Links, Image Button, etc.), the Styles dropdown is specific to the content selected. The above example is showing examples of text styles: you can see many more examples in the online Pattern Library. When the content you wish to style is selected, the different styling options available for that content are displayed in the Styles dropdown. 
Note: To remove a style you have applied, select the styled content and then go back to the Styles dropdown and select the applied styled. (e.g. You want to remove Lead styling: select the text, click the Styles dropdown and select ‘Lead’ – the text will revert to normal).
(L) Format Dropdown
The Format dropdown applies classes to tags (which styles them). 

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

1) Select the text you want to format.
2) Select the format from the Format dropdown.
Note: To remove a format you have applied, select the formatted content and then use the Format dropdown to select ‘Normal’.
 (M) Source
Click ‘Source’ button to toggle from the WYSIWYG view to see the HTML mark up. There are rare instances in which you will have to add tags to content in the Source view to format or style it: see Styled Blockquote for an example. 
 (N) Special Characters Button
You can use the ‘Special Character’ button to insert special characters if you do not know the keyboard shortcuts.

![FPO Screenshot Image Alt](img/fpo-screenshot.png)
*Image caption here*

1) With cursor in the WYSIWYG field, click ‘Special Character’ button.
2) Click on the special character you want to insert.
