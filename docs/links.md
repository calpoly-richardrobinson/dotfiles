# Links

Here are some links I've found on editing documentation hosted by readthedocs with MkDocs:

https://www.mkdocs.org/user-guide/writing-your-docs/
https://mkdocs.readthedocs.io/en/stable/#mkdocs

Here is a link to a talk on readthedocs:
https://www.youtube.com/watch?v=U6ueKExLzSY

Bootstrap Datepicker documentation on RTD:
https://bootstrap-datepicker.readthedocs.io/en/stable/index.html
