# Landing Page Components  

 A screenshot of a cell phone

Description automatically generated 

Button List
 A screenshot of a cell phone

Description automatically generated 

1) Enter (optional) intro content in the WYSIWYG field.
2) Enter URLs and Link Text for your button links.
The buttons can be reordered by either setting the row weight to the right, or clicking the blue ‘Hide row weights’ link and using the intersecting arrows icon on the left and dragging and dropping them into the desired order.
Content List Award
 A screenshot of a cell phone

Description automatically generated 

1) Select Award Type from dropdown.
2) Enter Title to appear above awards.
The ‘View More’ button is added automatically; clicking on it takes the user to the Rankings and Awards page.
Content List College
 A group of people posing for a photo

Description automatically generated 

1) Enter optional content text to appear above list of colleges.
Note: Upon rollover, a screen is applied over the image, text moves up and ‘View College’ button appears (example: Orfalea College of Business above). 
Content List Events
 A screenshot of a cell phone

Description automatically generated 

1) Select Event Type from dropdown.
Notes: 
The ‘View All Upcoming Events’ button is added automatically; clicking on it takes the user to the Events site.
The ‘Cal Poly Events’ title is entered automatically.
Content List Historical Facts
 A screenshot of a cell phone

Description automatically generated 

1) Select Historical Fact category from dropdown.
2) Enter Title appear above Historical Facts timeline.
Content List News
 A screenshot of a social media post

Description automatically generated 

1) Select News Category from dropdown.
Note: ‘View All Cal Poly News’ button is added automatically; clicking it takes the user to the News home page.
Curated Stories
 A screenshot of a social media post

Description automatically generated 

1) Enter optional intro text.
2) Add image for first card/story (see Adding Media to the Site > Images). This image should be the same dimensions as the image used for the third story.
3) Enter text in the Content field if you are entering simple text, with no styling or links; use the Content HTML field if you need to use features in the WYSIWYG editor. The text should run to the same number of lines as that in the third story.
4) Enter optional URL and link text. (Note: While the links are optional, this component was intended to link to other content.)
--Repeat for two other cards.
Note: It is important that the first and third cards have images that are the same size and copy that runs the same number of lines. The component will look off-balance (and unprofessional) otherwise.
Factoid List
 A picture containing grass, baseball, man, game

Description automatically generated 

The Factiod list is made up of individual Factoid blocks. The blocks will be centered on the page and after four they will wrap to the next line. 
 A screenshot of a cell phone

Description automatically generated 

The screenshot above is not very attractive but is included as an example to show you how the different fields display (the field name is followed by the option selected in brackets) and to demonstrate how careful you need to be when creating a Factoid list, lest you end up with one that looks as bad as this one does!
1) Add image (see Adding Media to the Site > Images). These are rarely used and are cropped in a circle: see last block in the list above for an example.
2) Enter Intro content.
3) Enter Value text. 
4) Select Value text size from the dropdown.
5) Enter Unit text.
6) Enter Context text.
7) Select Factoid Background (background options are called out above).
Featured Historical Fact
 A close up of a mans face

Description automatically generated 

1) Add Image (see Adding Media to the Site > Images).
2) Enter Date.
3) Enter Title.
4) Enter Content.
5) Enter URL and link text.
Featured Quote
 A screenshot of a computer

Description automatically generated 

1) Add image (see Adding Media to the Site > Images).
2) Enter Quote text.
3) Enter Quoter text.
4) Enter Quote label. This will most often be the major of the person quoted and the year of graduation.
5) Enter Quoter description. 
Full-Width Video or Image w/Content
 A sunset over a beach

Description automatically generated 

1) Add image (see Adding Media to the Site > Images).
2) Enter text that appears to the right on the image: this should be minimal; ideally no more than two lines. 
3) Enter caption that appears below the image. Note that in most cases you will not need/use the caption: this an option in only because the text to the right of the image must be brief.
Generic Content Grid
The Generic Content Grid is a very versatile landing page component, and can look quite different depending on the options you select. Below are a few examples.
 A screenshot of a cell phone

Description automatically generated 

* Simple headline in the optional intro area with no intro copy
* Left-aligned content in the cards
* No images
 A screenshot of a cell phone

Description automatically generated 

* Headline and intro copy in optional intro area
* Center-aligned content in the cards
* Images w/rounded corners (icons, in this example: note that photos w/people in them do not work well for round images)


 A screenshot of a social media post

Description automatically generated 

* Headline w/brief intro copy in optional intro area
* Left-aligned content in the cards
* Images
Note: These display in a grid of three across, and you can have as many rows as you’d like. If there are less than three cards in a row the cards will center-align.
1) In the Content Align dropdown, select either ‘Left ‘or ‘Center’.
2) Enter Intro content.
3) Add image for first card (see Adding Media to the Site > Images).
4) Check ‘Image with round corner’ if you want your image cropped in a circle. Note that this can only be done with center-aligned content.
5) Enter content for card.
6) Click ‘Add Generic Card’ button to add another card.
Horizontal Tabs
 A screenshot of a cell phone

Description automatically generated 

1) Enter tab Title.
2) Enter content to be displayed in tab in the Content field.
3) Click ‘Add Horizontal Tab’ to add another tab.Ideally, you will not have more than four tabs.
Image Banner
 A screenshot of a cell phone

Description automatically generated 

An Image Banner is a full-width image that is helpful in breaking up content on a long scrolling page, especially when two components you want to use won’t look good stacked on the page (as in the above example).
1) Add image (see Adding Media to the Site > Images).
Images Gallery
 A close up of many different types of food

Description automatically generated 

While the Images Gallery component uses fields you have seen in other templates and components -- a WYSIWYG intro area and a field to upload images -- it is a bit different as only images uploaded as Image with Position can be used.
1) Enter Intro content.
 A screenshot of a social media post

Description automatically generated 

2) Add Image, either by clicking ‘Media add page’ link or selecting it by using the type-ahead field. 
--the following steps assume you are uploading the image
3) Enter image Name. This is what will appear in the list on /admin/content/media.
4) Select Image Orientation from the dropdown.
5) Enter Alternative Text. Note that this will also appear in the Images Gallery as the photo caption that the user sees when hovering over the image.
6) Click ‘Save’ to save your work. This will take you back to the edit screen on the page you are adding the Images Gallery to.
7) Click ‘Add another item’ button to add more images.
Major Finder
 A screenshot of a cell phone

Description automatically generated 

To add the Major Finder to a page, simply select it from the dropdown: there is no content to enter and there are no settings to select.
Simple Content
 A screenshot of a newspaper

Description automatically generated 

The Simple Content component is simply a WYSIWYG field, with the option to center- or left-align your content. Above are example of each.
1) Enter content in WYSIWYG.
2) From the Content Align dropdown, select Left or Centered.
Split Content
 A screenshot of a cell phone

Description automatically generated 

1) Enter content in Content Left field.
2) Enter content in Content Right field.
Step-by-Step
 A screenshot of a social media post

Description automatically generated 

This content type is basically the same as the Generic Content Grid, with the addition of the shield that contains the number. Content is entered in cards that display three per row; if there are less than three cards in a row they will be centered.
1) Enter Intro content.
2) Add image (see Adding Media to the Site > Images).
3) Enter Eyebrow text.
4) Enter Title.
5) Enter card content. Use the Content field if you are entering only text, or the Content HTML field if you want to use add styling and format available via the WYSIWYG editor.
6) Add link URL and Link text.
7) Click ‘Add Card’ button to add another card
Note: Images, like all fields that do on have a red asterisk next to the label, are optional.
Two Columns with Image List
There are two variations of the Two Columns with Image List, the most widely used component on landing pages.
Row Style: Content Style
 A screenshot of a social media post

Description automatically generated 

In the above example, ‘Content Style’ has been selected from the Row Style dropdown and the Image Position is Left. You can also add another Two Column with Image below this and set the Image Position to Right to create an alternating grid layout.
Row Style: Full-Width
 A person sitting in a field

Description automatically generated 

With Row Style: ‘Full-Width’ selected, the text is displayed over a green background and both the text block and the image go the full-width of the page. You cannot add stack Two Column with Image components when ‘Full-Width’ is selected, however there is an optional Intro content area.
1) Enter Intro content in the Content Top WYSIWYG field.
2) Select Row Style: ‘Content Style’ for black text over white background; ‘Full-Width’ for white text over green background and full-width component.
3) Enter copy that will appear next to the image in the Content field.
4) Add image (see Adding Media to the Site > Images).
5) Set image to appear on the left or the right using the Image Position dropdown.
6) To add another Two Columns with Image List row, click ‘Add OLD—Two Columns with Image Row’ button. Reminder: you can only stack this component if you have selected the Content Style row style.

