# The Pattern Library

![Screenshot of the pattern library home page](img/pattern-library/1.png)
*Image caption here*

The Pattern Library is a visual design reference for the colors, pages, components and elements on the site. As of April 2020 it is hosted by Rolling Orange but the URL will change when it is moved to a Cal Poly server. 

Please refer to the Pattern Library for image guidelines. Image dimensions for the different components are called out in a green box below the different components.

![FPO Screenshot Image](img/fpo-screenshot.png)
*Image caption here*
