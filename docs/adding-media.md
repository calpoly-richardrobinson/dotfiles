# Adding Media to the Site

While you can add files, images and video in the Body field of many content types, featured and hero images and images for most components need to be uploaded to the site and then selected via a type-ahead field in order to be added to the page. The process is the same for each: as you will most often be uploading regular images that is what is shown here. 
Images

![FPO Screenshot Image](img/fpo-screenshot.png)
*Image caption here*

1) From the top Admin menu, select Content > Add Media > Image.

![FPO Screenshot Image](img/fpo-screenshot.png)
*Image caption here*

2) Enter a name for the file. This can just be the file name, or you can rename it to describe, for example, where in the site it lives.
3) Click ‘Chose File’ to upload image. (Until you upload the image this field will appear as it does in the upper right corner of the screenshot; the thumbnail image and the Alternative text field are displayed after the image is uploaded.)
4) Enter Alternative text.
5) Click ‘Save’ to save your work.
* Image with Position should be used when uploading images in an image gallery on a landing page. This will allow you to select the image orientation -- ‘Landscape’ or ‘Portrait’ -- which will position the images correctly in relation to each other in the gallery.
Files / Media
See instructions above: the process for uploading files or media is the same as it is for images.
